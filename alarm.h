#ifndef ALARM_H
#define ALARM_H

#include <QDateTime>
#include <QTime>
#include <QObject>

class Alarm : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool active READ isActive WRITE setActive NOTIFY activeChanged)
    Q_PROPERTY(QTime alarmTime READ alarmTime WRITE setAlarmTime NOTIFY alarmTimeChanged)
    Q_PROPERTY(Weekdays weekdays READ weekdays WRITE setWeekdays NOTIFY weekdaysChanged)
public:
    enum Weekday {
        None = 0x0,
        Monday = 0x01,
        Tuesday = 0x02,
        Wednesday = 0x04,
        Thursday = 0x08,
        Friday = 0x10,
        Saturday = 0x20,
        Sunday = 0x40,

        WorkingDays = Monday | Tuesday | Wednesday | Thursday | Friday,
        Weekend = Saturday | Sunday,
        AllWeekdays = Monday | Tuesday | Wednesday | Thursday | Friday | Saturday | Sunday
    };
    Q_DECLARE_FLAGS(Weekdays, Weekday)

    explicit Alarm(QObject *parent = 0);

    bool isActive() const;
    QTime alarmTime() const;
    Weekdays weekdays() const;

    QDateTime nextAlarm(const QDateTime &now = QDateTime::currentDateTime()) const;

signals:
    void activeChanged(bool active);

    void alarmTimeChanged(const QTime &alarmTime);
    void weekdaysChanged(const Weekdays &weekdays);

public slots:
    void setActive(bool active);

    void setAlarmTime(const QTime &alarmTime);
    void setWeekdays(const Weekdays &weekdays);

private:
    bool m_active;
    QTime m_alarmTime;
    Weekdays m_weekdays;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(Alarm::Weekdays)

#endif // ALARM_H
