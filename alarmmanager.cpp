#include "alarmmanager.h"

#include "alarm.h"

#include <QDateTime>
#include <QSettings>
#include <QStateMachine>
#include <QState>
#include <QTimer>

#include <algorithm>

AlarmManager::AlarmManager(QObject *parent)
    : QObject(parent), m_alarmUpdateTimer(new QTimer(this)), m_snoozeTimer(new QTimer(this)),
      m_stateMachine(new QStateMachine(this)), m_state(Idle)
{
    m_alarmUpdateTimer->setInterval(1000);
    connect(m_alarmUpdateTimer, &QTimer::timeout,
            this, &AlarmManager::checkAlarmTimes);
    m_alarmUpdateTimer->start();

    connect(this, &AlarmManager::alarmsChanged, this, &AlarmManager::checkAlarmTimes);

    QSettings settings;
    m_snoozeTimer->setInterval(settings.value("alarm/snoozeTimeout", 5000 * 60).toInt());
    m_snoozeTimer->setSingleShot(true);
    connect(m_snoozeTimer, &QTimer::timeout, this, &AlarmManager::ring);

    QState *idle = new QState(m_stateMachine),
           *ringing = new QState(m_stateMachine),
           *snoozing = new QState(m_stateMachine);

    connect(idle, &QState::entered, this, [this]() { m_state = Idle; emit stateChanged(m_state); });
    connect(ringing, &QState::entered, this, [this]() { m_state = Ring; emit stateChanged(m_state); });
    connect(snoozing, &QState::entered, this, [this]() { m_state = Snooze; emit stateChanged(m_state); });

    connect(snoozing, &QState::entered, this, [this]() { m_snoozeTimer->start(); });

    idle->addTransition(this, &AlarmManager::ring, ringing);
    snoozing->addTransition(this, &AlarmManager::ring, ringing);
    snoozing->addTransition(this, &AlarmManager::idle, idle);
    ringing->addTransition(this, &AlarmManager::idle, idle);

    m_stateMachine->setInitialState(idle);
    m_stateMachine->start();
}

QList<Alarm *> AlarmManager::alarms() const
{
    return m_alarms;
}

void AlarmManager::addAlarm(Alarm *alarm)
{
    if (m_alarms.contains(alarm))
        return;

    m_alarms.append(alarm);

    emit alarmsChanged(m_alarms);
}

void AlarmManager::removeAlarm(Alarm *alarm)
{
    int removed = m_alarms.removeAll(alarm);

    if (removed != 0) {
        emit alarmsChanged(m_alarms);
    }
}

QDateTime AlarmManager::nextAlarm() const
{
    if (m_nextAlarmQueue.isEmpty())
        return QDateTime();

    return m_nextAlarmQueue.firstKey();
}

AlarmManager::State AlarmManager::state() const
{
    return m_state;
}

void AlarmManager::setAlarms(const QList<Alarm *> &alarms)
{
    if (m_alarms == alarms)
        return;

    m_alarms = alarms;
    emit alarmsChanged(alarms);
}

void AlarmManager::checkAlarmTimes()
{
    const QDateTime now = QDateTime::currentDateTime();

    const QDateTime oldNextAlarm = nextAlarm();

    for (auto it = m_nextAlarmQueue.constBegin(); it != m_nextAlarmQueue.constEnd(); ++it) {
        if (it.key() < now) {
            emit ring();
            break;
        }
    }

    m_nextAlarmQueue.clear();

    for (Alarm *alarm : m_alarms) {
        if (!alarm->isActive()) {
            continue;
        }
        m_nextAlarmQueue.insert(alarm->nextAlarm(now), alarm);
    }

    if (nextAlarm() != oldNextAlarm) {
        emit nextAlarmChanged(nextAlarm());
    }
}
