#ifndef ALARMEDITOR_H
#define ALARMEDITOR_H

#include <QWidget>

namespace Ui {
class AlarmEditor;
}

class Alarm;

class AlarmEditor : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(Alarm *alarm READ alarm WRITE setAlarm NOTIFY alarmChanged)
public:
    explicit AlarmEditor(QWidget *parent = 0);
    ~AlarmEditor();

    Alarm *alarm() const;

public slots:
    void setAlarm(Alarm *alarm);
    void apply();

signals:
    void alarmChanged(Alarm *alarm);
    void removed();
    void applied();
    void hidden();

protected:
    void hideEvent(QHideEvent *);
    void focusInEvent(QFocusEvent*);
    bool eventFilter(QObject *obj, QEvent *e);

private:
    Ui::AlarmEditor *ui;
    Alarm *m_alarm;
};

#endif // ALARMEDITOR_H
