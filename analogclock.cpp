#include "analogclock.h"

#include <QPainter>
#include <QSvgRenderer>
#include <QTime>

#include <QtDebug>

AnalogClock::AnalogClock(QWidget *parent)
    : QWidget(parent), m_renderer(0), m_time(QTime::currentTime()), m_secondHandVisible(false)
{
}

AnalogClock::AnalogClock(QSvgRenderer *renderer, QWidget *parent)
    : QWidget(parent), m_renderer(renderer), m_time(QTime::currentTime()), m_secondHandVisible(false)
{
}

QSvgRenderer *AnalogClock::renderer() const
{
    return m_renderer;
}

QSize AnalogClock::sizeHint() const
{
    if (!m_renderer) {
        return QSize();
    }

    return m_renderer->defaultSize();
}

QTime AnalogClock::time() const
{
    return m_time;
}

bool AnalogClock::secondHandVisible() const
{
    return m_secondHandVisible;
}

void AnalogClock::setRenderer(QSvgRenderer *renderer)
{
    if (m_renderer == renderer)
        return;

    m_renderer = renderer;
    emit rendererChanged(renderer);
    update();
}

void AnalogClock::setTime(const QTime &time)
{
    if (m_time == time)
        return;

    m_time = time;
    emit timeChanged(time);
    update();
}

void AnalogClock::setSecondHandVisible(bool secondHandVisible)
{
    if (m_secondHandVisible == secondHandVisible)
        return;

    m_secondHandVisible = secondHandVisible;
    emit secondHandVisibleChanged(secondHandVisible);
    update();
}

void AnalogClock::paintEvent(QPaintEvent *)
{
    if (!m_renderer)
        return;

    QPainter p(this);

    QRectF clockBounds = m_renderer->boundsOnElement("ClockFace");
    clockBounds.moveTopLeft(QPointF(0, 0));

    qreal sx = width() / clockBounds.width();
    qreal sy = height() / clockBounds.height();

    qreal s = qMin(sx, sy);

    qreal dx = (width() - clockBounds.width() * s) / 2;
    qreal dy = (height() - clockBounds.height() * s) / 2;

    p.translate(dx, dy);

    p.scale(s, s);

    m_renderer->render(&p, "ClockFace", clockBounds);
    p.translate(clockBounds.center());

    static const qreal ShadowOffset = 5.0;

    qreal hourAngle = 180 + m_time.hour() * (360.0 / 12.0) + (m_time.minute() / 60.0 * 360.0 / 12.0);
    drawHand(p, "HourHandShadow", hourAngle, ShadowOffset);
    drawHand(p, "HourHand", hourAngle);

    qreal minuteAngle = 180 + m_time.minute() * 360.0 / 60.0;
    drawHand(p, "MinuteHandShadow", minuteAngle, ShadowOffset);
    drawHand(p, "MinuteHand", minuteAngle);

    if (m_secondHandVisible) {
        qreal secondAngle = 180 + m_time.second() * 360.0 / 60.0;
        drawHand(p, "SecondHandShadow", secondAngle, ShadowOffset);
        drawHand(p, "SecondHand", secondAngle);
    }

    QRectF centerScrewBounds = m_renderer->boundsOnElement("HandCenterScrew");
    centerScrewBounds.moveCenter(QPointF(0, 0));
    m_renderer->render(&p, "HandCenterScrew", centerScrewBounds);
}

void AnalogClock::drawHand(QPainter &p, const QString &id, qreal rot, qreal topMargin)
{
    QRectF handBounds = m_renderer->boundsOnElement(id);
    handBounds.moveLeft(-handBounds.width() / 2);
    handBounds.moveTop(-handBounds.width() / 2);
    p.save();
    p.translate(0, topMargin);
    p.rotate(rot);
    m_renderer->render(&p, id, handBounds);
    p.restore();
}
