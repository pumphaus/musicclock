#ifndef PLAYERBASE_H
#define PLAYERBASE_H

#include <QObject>

class PlayerBase : public QObject
{
    Q_OBJECT
    Q_PROPERTY(PlayerBase::State state READ state NOTIFY stateChanged)
    Q_PROPERTY(QString title READ title NOTIFY titleChanged)
public:
    enum State {
        Idle,
        Playing,
        Paused,
        Error
    };
    Q_ENUM(State)

public:
    explicit PlayerBase(QObject *parent = 0);

    virtual PlayerBase::State state() const = 0;
    virtual QString title() const = 0;

signals:
    void stateChanged(PlayerBase::State state);
    void titleChanged(const QString &title);

public slots:
    virtual void play() = 0;
    virtual void pause() = 0;
    virtual void stop() = 0;
};

#endif // PLAYERBASE_H
