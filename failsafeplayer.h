#ifndef FAILSAFEPLAYER_H
#define FAILSAFEPLAYER_H

#include "playerbase.h"

#include <QMediaPlayer>

class AlarmManager;

class FailsafePlayer : public PlayerBase
{
    Q_OBJECT
public:
    explicit FailsafePlayer(AlarmManager *manager, PlayerBase *player, QObject *parent = 0);

    virtual PlayerBase::State state() const override;
    virtual QString title() const override;

public slots:
    virtual void play() override;
    virtual void pause() override;
    virtual void stop() override;

private:
    void checkFallbackAlarm();

    AlarmManager *m_alarmManager;
    PlayerBase *m_player;
    QMediaPlayer *m_fallbackPlayer;
};

#endif // FAILSAFEPLAYER_H
