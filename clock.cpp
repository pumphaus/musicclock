#include "clock.h"
#include "ui_clock.h"

#include <QPainter>
#include <QSvgRenderer>
#include <QtDebug>

#include <QGraphicsDropShadowEffect>

#include "alarm.h"
#include "alarmmanager.h"
#include "analogclock.h"
#include "playerbase.h"

// possible symbols: 🔔, 🔕, ⏰, 🕭, ⏵, ⏸, ⏹
static const QString alarmOn = QString::fromUtf8("⏰");
static const QString alarmOff = QString::fromUtf8("");

Clock::Clock(PlayerBase *player, AlarmManager *alarmManager, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Clock),
    m_clockRenderer(new QSvgRenderer(QString("clock.svgz"), this)),
    m_alarmIndicator(new QLabel(this)), m_titleLabel(new QLabel(this)),
    m_alarmManager(alarmManager),
    m_player(player)
{
    ui->setupUi(this);

    ui->analogClock->setRenderer(m_clockRenderer);

    ui->timeLabel->setGraphicsEffect(createDropShadowEffect());

    QPalette alarmPalette = ui->alarmLabel->palette();
    alarmPalette.setColor(QPalette::WindowText, alarmPalette.color(QPalette::WindowText).darker(110));
    ui->alarmLabel->setPalette(alarmPalette);
    ui->alarmLabel->setGraphicsEffect(createDropShadowEffect(1.4));

    m_alarmIndicator->setPalette(alarmPalette);
    m_alarmIndicator->setGraphicsEffect(createDropShadowEffect(1.4));
    QFont alarmFont = ui->alarmLabel->font();
    alarmFont.setPointSizeF(alarmFont.pointSizeF() * 0.7);
    m_alarmIndicator->setFont(alarmFont);
    QFontMetrics metrics = m_alarmIndicator->fontMetrics();
    QRect bRect = metrics.boundingRect(alarmOn).united(metrics.boundingRect(alarmOff));
    m_alarmIndicator->setFixedSize(bRect.size());
    m_alarmIndicator->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

    connect(this, &Clock::timeChanged, ui->analogClock, &AnalogClock::setTime);
    ui->alarmLabel->installEventFilter(this);

    setTime(QTime::currentTime());

    ui->analogClock->setSecondHandVisible(true);

    m_alarmIndicator->setText(alarmOn);
    m_alarmIndicator->setVisible(false);
    ui->alarmLabel->setVisible(false);
    connect(m_alarmManager, &AlarmManager::nextAlarmChanged, this, &Clock::setNextAlarm);

    m_titleLabel->setMargin(6);
    m_titleLabel->setAlignment(Qt::AlignBottom | Qt::AlignHCenter);
    m_titleLabel->setWordWrap(true);
    m_titleLabel->setGraphicsEffect(createDropShadowEffect(1.4));

    connect(player, &PlayerBase::titleChanged, m_titleLabel, &QLabel::setText);

    m_titleLabel->setVisible(player->state() == PlayerBase::Playing);
    connect(player, &PlayerBase::stateChanged,
            this, [this](PlayerBase::State state)
    {
        m_titleLabel->setVisible(state == PlayerBase::Playing);
    });
}

Clock::~Clock()
{
    delete ui;
}

QTime Clock::time() const
{
    return m_time;
}

void Clock::setTime(const QTime &time)
{
    if (m_time == time)
        return;

    m_time = time;
    ui->timeLabel->setText(m_time.toString("hh:mm"));
    emit timeChanged(time);
}

void Clock::setNextAlarm(const QDateTime &nextAlarm)
{
    if (!nextAlarm.isValid()) {
        ui->alarmLabel->setVisible(false);
        m_alarmIndicator->setVisible(false);
        return;
    }

    ui->alarmLabel->setText(nextAlarm.toString("ddd, HH:mm"));
    ui->alarmLabel->setVisible(true);
    m_alarmIndicator->setVisible(true);
}

bool Clock::eventFilter(QObject *, QEvent *e)
{
    if (e->type() == QEvent::Resize) {
        repositionAlarmIndicator();
        repositionTitleLabel();
    }
    return false;
}

QGraphicsEffect *Clock::createDropShadowEffect(qreal offset) const
{
    QGraphicsDropShadowEffect *dropShadowEffect = new QGraphicsDropShadowEffect;
    dropShadowEffect->setBlurRadius(3);
    dropShadowEffect->setOffset(offset);
    return dropShadowEffect;
}

void Clock::repositionAlarmIndicator()
{
    QRect bRect = m_alarmIndicator->geometry();
    bRect.moveCenter(ui->alarmLabel->geometry().center());
    bRect.moveRight(ui->alarmLabel->geometry().left() - 6);
    m_alarmIndicator->setGeometry(bRect);
}

void Clock::repositionTitleLabel()
{
    m_titleLabel->setGeometry(0, 0, width(), height());
}
