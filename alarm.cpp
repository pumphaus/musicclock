#include "alarm.h"

#include <QTimer>
#include <QSettings>

#include <QtDebug>

Alarm::Alarm(QObject *parent)
    : QObject(parent), m_active(true), m_alarmTime(QTime(0, 0)),
      m_weekdays(AllWeekdays)
{
}

bool Alarm::isActive() const
{
    return m_active;
}

QTime Alarm::alarmTime() const
{
    return m_alarmTime;
}

Alarm::Weekdays Alarm::weekdays() const
{
    return m_weekdays;
}

static int ipow (int x, int n) {
    int ret = 1;
    for (int i = 1; i <= n; i++) {
        ret *= x;
    }
    return ret;
}

QDateTime Alarm::nextAlarm(const QDateTime& now) const
{
    if (m_weekdays == None) {
        return QDateTime();
    }

    QDateTime next = now;
    next.setTime(m_alarmTime);
    if (next < now) {
        next = next.addDays(1);
    }

    while (true) {
        Weekday theWeekday = static_cast<Weekday>(ipow(2, next.date().dayOfWeek() - 1));
        if (m_weekdays.testFlag(theWeekday)) {
            return next;
        }
        next = next.addDays(1);
    }
}

void Alarm::setActive(bool active)
{
    if (m_active == active)
        return;

    m_active = active;
    emit activeChanged(active);
}

void Alarm::setAlarmTime(const QTime &alarmTime)
{
    if (m_alarmTime == alarmTime)
        return;

    m_alarmTime = alarmTime;
    emit alarmTimeChanged(alarmTime);
}

void Alarm::setWeekdays(const Weekdays &weekdays)
{
    if (m_weekdays == weekdays)
        return;

    m_weekdays = weekdays;
    emit weekdaysChanged(weekdays);
}
