#ifndef CLOCKMENU_H
#define CLOCKMENU_H

#include <QWidget>

class QVBoxLayout;

class ClockMenu : public QWidget
{
    Q_OBJECT

public:
    explicit ClockMenu(QWidget *parent = 0);
    ~ClockMenu();

public slots:
    void repopulateButtons();

protected:
    void focusInEvent(QFocusEvent *);

private:
    QVBoxLayout *m_layout;
};

#endif // CLOCKMENU_H
