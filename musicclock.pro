#-------------------------------------------------
#
# Project created by QtCreator 2016-03-02T23:11:19
#
#-------------------------------------------------

QT       += core gui svg multimedia

CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = musicclock
TEMPLATE = app

SOURCES += main.cpp\
    clock.cpp \
    analogclock.cpp \
    musicclockappwidget.cpp \
    clockmenu.cpp \
    alarmeditor.cpp \
    alarmmanager.cpp \
    alarm.cpp \
    webradioplayer.cpp \
    mpdclient.cpp \
    playerbase.cpp \
    failsafeplayer.cpp

HEADERS  += clock.h \
    analogclock.h \
    musicclockappwidget.h \
    clockmenu.h \
    alarmeditor.h \
    alarmmanager.h \
    alarm.h \
    webradioplayer.h \
    mpdclient.h \
    playerbase.h \
    failsafeplayer.h

FORMS    += clock.ui \
    alarmeditor.ui
