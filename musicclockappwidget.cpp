#include "musicclockappwidget.h"
#include "clock.h"
#include "clockmenu.h"

#include <QApplication>
#include <QEvent>
#include <QKeyEvent>
#include <QGraphicsBlurEffect>
#include <QLabel>
#include <QMenu>
#include <QTimer>
#include <QProxyStyle>
#include <QScrollArea>
#include <QScrollBar>
#include <QShortcut>
#include <QStackedWidget>

#include "alarm.h"
#include "alarmeditor.h"
#include "alarmmanager.h"

#include <QtDebug>

MusicClockAppWidget::MusicClockAppWidget(PlayerBase *player,
                                         AlarmManager *alarmManager,
                                         QWidget *parent)
    : QWidget(parent),
      m_alarmEditor(new AlarmEditor), m_alarmManager(alarmManager)
{
    setFixedSize(240, 320);

    QPalette pal = palette();
    pal.setColor(QPalette::Window, QColor(Qt::gray).darker());
    pal.setColor(QPalette::Text, QColor(Qt::white));
    pal.setColor(QPalette::ButtonText, QColor(Qt::white));
    pal.setColor(QPalette::WindowText, QColor(Qt::white));
    pal.setColor(QPalette::Base, QColor(Qt::transparent));

    QColor highlightColor(Qt::black);
    highlightColor.setAlpha(64);
    pal.setColor(QPalette::Highlight, highlightColor);
    setPalette(pal);

    QFont font = this->font();
    font.setPointSize(14);
    setFont(font);

    QLabel *bgLabel = new QLabel(this);
    bgLabel->setBackgroundRole(QPalette::WindowText);
    bgLabel->setGeometry(geometry());
    auto blurEffect = new QGraphicsBlurEffect;
    blurEffect->setBlurHints(QGraphicsBlurEffect::QualityHint);
    blurEffect->setBlurRadius(5);
    bgLabel->setGraphicsEffect(blurEffect);

    QImage background("bg.jpg");
    background = background.scaled(bgLabel->size(), Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation);
    bgLabel->setPixmap(QPixmap::fromImage(background));

    QLabel *dimLabel = new QLabel(this);
    dimLabel->setGeometry(geometry());
    dimLabel->setBackgroundRole(QPalette::Background);
    pal = dimLabel->palette();
    QColor darkAlpha(Qt::black);
    darkAlpha.setAlpha(48);
    pal.setColor(QPalette::Background, darkAlpha);
    dimLabel->setPalette(pal);
    dimLabel->setAutoFillBackground(true);

    m_stack = new QStackedWidget(this);
    m_stack->setGeometry(geometry());

    qApp->setProperty("widgetStack", QVariant::fromValue(m_stack));

    Clock *clock = new Clock(player, m_alarmManager, m_stack);
    m_stack->addWidget(clock);

    QTimer *updateTimer = new QTimer(this);
    connect(updateTimer, &QTimer::timeout, [clock](){ clock->setTime(QTime::currentTime()); });
    updateTimer->start(1000);

    clock->show();

    ClockMenu *menu = new ClockMenu(m_stack);
    menu->hide();

    QAction *alarmTimesAction = new QAction("Weckzeiten", menu);
    connect(alarmTimesAction, &QAction::triggered, this, &MusicClockAppWidget::showAlarmsMenu);
    menu->addAction(alarmTimesAction);

    m_alarmsMenu = new ClockMenu(m_stack);
    m_alarmsMenu->hide();
    QAction *newAlarmAction = new QAction("Neue Weckzeit", m_alarmsMenu);
    newAlarmAction->setProperty("persistent", true);
    connect(newAlarmAction, &QAction::triggered, this, &MusicClockAppWidget::showNewAlarmDialog);
    m_alarmsMenu->addAction(newAlarmAction);

    QAction *fmRadioAction = new QAction("FM Radio", menu);
    connect(fmRadioAction, &QAction::triggered, this, &MusicClockAppWidget::showFMRadio);
    menu->addAction(fmRadioAction);

    QAction *webRadioAction = new QAction("Webradio", menu);
    connect(webRadioAction, &QAction::triggered, this, &MusicClockAppWidget::showWebRadio);
    menu->addAction(webRadioAction);

    menu->repopulateButtons();

    QAction *escapeAction = new QAction(this);
    escapeAction->setShortcut(QKeySequence(Qt::Key_Escape));
    connect(escapeAction, &QAction::triggered, this, &MusicClockAppWidget::popCurrentWidget);
    addAction(escapeAction);

    QAction *showClockMenu = new QAction(clock);
    showClockMenu->setShortcut(QKeySequence(Qt::Key_Space));
    connect(showClockMenu, &QAction::triggered, [this, menu](){
        m_stack->addWidget(menu);
        m_stack->setCurrentWidget(menu);
        menu->setFocus();
    });
    clock->addAction(showClockMenu);

    connect(m_alarmEditor, &AlarmEditor::applied, this, &MusicClockAppWidget::popCurrentWidget);
    connect(m_alarmEditor, &AlarmEditor::removed, this, &MusicClockAppWidget::popCurrentWidget);
}

void MusicClockAppWidget::popCurrentWidget()
{
    if (m_stack->count() <= 1) {
        return;
    }
    m_stack->removeWidget(m_stack->currentWidget());
}

void MusicClockAppWidget::repopulateAlarmsMenu()
{
    static const QString alarmOn = QStringLiteral("⏰ ");

    for (QAction *action : m_alarmsMenu->actions()) {
        if (!action->property("persistent").toBool()) {
            m_alarmsMenu->removeAction(action);
            delete action;
        }
    }

    for (Alarm *alarm : m_alarmManager->alarms()) {
        QString alarmText = (alarm->isActive() ? alarmOn : QString()) + alarm->alarmTime().toString("HH:mm");
        QAction *alarmAction = new QAction(alarmText, alarm);
        connect(alarmAction, &QAction::triggered, [this, alarm](){
            showModifyAlarmDialog(alarm);
        });
        m_alarmsMenu->addAction(alarmAction);
    }

    m_alarmsMenu->repopulateButtons();
}

void MusicClockAppWidget::showAlarmsMenu()
{
    repopulateAlarmsMenu();
    m_stack->addWidget(m_alarmsMenu);
    m_stack->setCurrentWidget(m_alarmsMenu);
}

void MusicClockAppWidget::showNewAlarmDialog()
{
    Alarm *alarm = new Alarm(m_alarmManager);
    m_alarmEditor->setAlarm(alarm);
    m_stack->addWidget(m_alarmEditor);
    m_stack->setCurrentWidget(m_alarmEditor);
    m_alarmEditor->setFocus();

    QEventLoop loop;

    connect(m_alarmEditor, &AlarmEditor::applied, &loop, [&](){
        m_alarmManager->addAlarm(alarm);
        repopulateAlarmsMenu();
    });
    connect(m_alarmEditor, &AlarmEditor::hidden, &loop, &QEventLoop::quit);

    loop.exec();

    if (!m_alarmManager->alarms().contains(alarm)) {
        delete alarm;
    }

    m_alarmEditor->setAlarm(0);
    m_stack->currentWidget()->setFocus();
}

void MusicClockAppWidget::showModifyAlarmDialog(Alarm *alarm)
{
    m_alarmEditor->setAlarm(alarm);
    m_stack->addWidget(m_alarmEditor);
    m_stack->setCurrentWidget(m_alarmEditor);
    m_alarmEditor->setFocus();

    QEventLoop loop;

    connect(m_alarmEditor, &AlarmEditor::applied, &loop, [&](){
        repopulateAlarmsMenu();
    });
    connect(m_alarmEditor, &AlarmEditor::removed, &loop, [&](){
        m_alarmManager->removeAlarm(alarm);
        repopulateAlarmsMenu();
        delete alarm;
    });
    connect(m_alarmEditor, &AlarmEditor::hidden, &loop, &QEventLoop::quit);

    loop.exec();

    m_alarmEditor->setAlarm(0);
    m_stack->currentWidget()->setFocus();
}

void MusicClockAppWidget::showFMRadio()
{

}

void MusicClockAppWidget::showWebRadio()
{

}
