#ifndef MPDCLIENT_H
#define MPDCLIENT_H

#include "playerbase.h"

#include <QMap>
#include <QQueue>

class QTcpSocket;

class MPDClient : public PlayerBase
{
    Q_OBJECT
public:
    explicit MPDClient(QObject *parent = 0);
    ~MPDClient();

    PlayerBase::State state() const override;
    QString title() const override;

signals:
    void mpdReplyReady(const QByteArray &command,
                       const QMap<QByteArray, QByteArray> &reply);

public slots:
    void play() override;
    void pause() override;
    void stop() override;

private slots:
    void initConnection();
    void failedConnection();
    void mpdDataAvailable();

    void sendCommand(const QByteArray &command);
    void processReply(const QByteArray &command,
                      const QMap<QByteArray, QByteArray> &reply);


private:
    void setState(State state);
    void setTitle(const QString &title);

    MPDClient::State m_state;
    QString m_title;
    QTcpSocket *m_sock;

    QQueue<QByteArray> m_commandQueue;
    QMap<QByteArray, QByteArray> m_mpdReply;
};

#endif // MPDCLIENT_H
