#include "mpdclient.h"

#include <QEventLoop>
#include <QThread>
#include <QTcpSocket>

#include <QtDebug>

MPDClient::MPDClient(QObject *parent)
    : PlayerBase(parent), m_state(Error)
{
    m_sock = new QTcpSocket(this);

    m_sock->connectToHost("localhost", 6600);
    connect(m_sock, &QTcpSocket::connected, this, &MPDClient::initConnection);
    connect(m_sock, static_cast<void(QTcpSocket::*)(QAbstractSocket::SocketError)>(&QTcpSocket::error),
            this, &MPDClient::failedConnection);
    connect(m_sock, &QTcpSocket::disconnected, this, &MPDClient::failedConnection);
    connect(m_sock, &QTcpSocket::readyRead, this, &MPDClient::mpdDataAvailable);

    connect(this, &MPDClient::mpdReplyReady, this, &MPDClient::processReply);
}

MPDClient::~MPDClient()
{
}

MPDClient::State MPDClient::state() const
{
    return m_state;
}

QString MPDClient::title() const
{
    return m_title;
}

void MPDClient::play()
{
    qDebug() << "Sending play...";
    sendCommand("noidle");
    sendCommand("play");
    sendCommand("idle");
}

void MPDClient::pause()
{
    qDebug() << "Sending pause...";
    sendCommand("noidle");
    sendCommand("pause");
    sendCommand("idle");
}

void MPDClient::stop()
{
    qDebug() << "Sending stop...";
    sendCommand("noidle");
    sendCommand("stop");
    sendCommand("idle");
}

void MPDClient::initConnection()
{
    setState(Idle);
    m_commandQueue.append("init");
    sendCommand("status");
    sendCommand("currentsong");
    sendCommand("idle");

    qDebug() << "Connected!";
}

void MPDClient::failedConnection()
{
    setState(Error);
}

void MPDClient::mpdDataAvailable()
{
    while (m_sock->canReadLine()) {
        QByteArray line = m_sock->readLine();
        if (line.startsWith(QByteArrayLiteral("OK"))) {
            if (!m_commandQueue.isEmpty()) {
                emit mpdReplyReady(m_commandQueue.dequeue(), m_mpdReply);
            }
            m_mpdReply.clear();
            continue;
        }
        QList<QByteArray> pairs = line.split(':');
        m_mpdReply[pairs[0].trimmed()] = pairs[1].trimmed();
    }
}

void MPDClient::sendCommand(const QByteArray &command)
{
    if (state() == Error) {
        qDebug() << "Discarding:" << command;
        return;
    }
    if (command != "noidle") {
        m_commandQueue.enqueue(command);
    }
    m_sock->write(command + '\n');
}

void MPDClient::processReply(const QByteArray &command,
                             const QMap<QByteArray, QByteArray> &reply)
{
    if (command == QByteArrayLiteral("idle")) {
        if (!reply.isEmpty()) {
            sendCommand("status");
            sendCommand("currentsong");
            sendCommand("idle");
        }
    } else if (command == QByteArrayLiteral("status")) {
        const QByteArray state = reply["state"];
        if (state == QByteArrayLiteral("play")) {
            setState(Playing);
        } else if (state == QByteArrayLiteral("pause")) {
            setState(Paused);
        } else if (state == QByteArrayLiteral("stop")) {
            setState(Idle);
        }
    } else if (command == QByteArrayLiteral("currentsong")) {
        QString title;
        for (auto field : { "Name", "Artist", "Title"}) {
            if (!reply.contains(field)) {
                continue;
            }
            if (!title.isEmpty()) {
                title += QStringLiteral(" - ");
            }
            title += QString::fromUtf8(reply[field]);
        }
        setTitle(title);
    }
}

void MPDClient::setState(MPDClient::State state)
{
    if (state == m_state) {
        return;
    }

    m_state = state;
    emit stateChanged(m_state);
}

void MPDClient::setTitle(const QString &title)
{
    if (title == m_title) {
        return;
    }

    m_title = title;
    emit titleChanged(m_title);
}
