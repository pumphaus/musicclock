#ifndef MUSICCLOCKAPPWIDGET_H
#define MUSICCLOCKAPPWIDGET_H

#include <QWidget>

class QStackedWidget;
class AlarmEditor;
class AlarmManager;
class ClockMenu;
class Alarm;
class PlayerBase;

class MusicClockAppWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MusicClockAppWidget(PlayerBase *player, AlarmManager *alarmManager, QWidget *parent = 0);

private slots:
    void popCurrentWidget();

    void repopulateAlarmsMenu();
    void showAlarmsMenu();
    void showNewAlarmDialog();
    void showModifyAlarmDialog(Alarm *alarm);

    void showFMRadio();
    void showWebRadio();

private:
    QStackedWidget *m_stack;
    AlarmEditor *m_alarmEditor;
    AlarmManager *m_alarmManager;
    ClockMenu *m_alarmsMenu;
};

#endif // MUSICCLOCKAPPWIDGET_H
