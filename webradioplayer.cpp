#include "webradioplayer.h"
#include <QFile>
#include <QMediaPlayer>
#include <QTimer>

#include <QtDebug>

WebRadioPlayer::WebRadioPlayer(QObject *parent)
    : QObject(parent), m_mediaPlayer(new QMediaPlayer(this)), m_restartTimer(new QTimer(this))
{
    connect(m_mediaPlayer, &QMediaPlayer::stateChanged, this, &WebRadioPlayer::stateChanged);
    connect(m_mediaPlayer, static_cast<void(QMediaPlayer::*)(QMediaPlayer::Error)>(&QMediaPlayer::error), this, &WebRadioPlayer::handleError);

    QFile stations("streams.txt");
    stations.open(QFile::ReadOnly);

    while (!stations.atEnd()) {
        QByteArray line = stations.readLine().trimmed();

        QList<QByteArray> stationNameAndUrl = line.split('=');
        if (stationNameAndUrl.count() != 2) {
            continue;
        }

        QString name = QString::fromUtf8(stationNameAndUrl[0]);
        QUrl url(QString::fromUtf8(stationNameAndUrl[1]));

        m_stations[name] = url;
    }

    qDebug() << m_stations;

    if (!m_stations.isEmpty()) {
        setStation(m_stations.firstKey());
    }

    m_restartTimer->setSingleShot(true);
    m_restartTimer->setInterval(3000);
    connect(m_restartTimer, &QTimer::timeout, m_mediaPlayer, &QMediaPlayer::play);
}

QStringList WebRadioPlayer::stations() const
{
    return m_stations.keys();
}

void WebRadioPlayer::setStation(const QString &station)
{
    if (m_station == station)
        return;

    if (!m_stations.contains(station)) {
        qWarning("WebRadioPlayer: Unknown station %s", qPrintable(station));
        return;
    }

    m_station = station;
    emit stationChanged(station);

    if (m_mediaPlayer->state() == QMediaPlayer::PlayingState) {
        play();
    }
}

void WebRadioPlayer::handleError(QMediaPlayer::Error error)
{
    // shouldn't happen
    if (error == QMediaPlayer::NoError) {
        return;
    }

    qDebug() << "Error occurred:" << error << "; restarting in " << m_restartTimer->interval();

    m_restartTimer->start();
}

QString WebRadioPlayer::station() const
{
    return m_station;
}

QMediaPlayer::State WebRadioPlayer::state() const
{
    return m_mediaPlayer->state();
}

void WebRadioPlayer::play()
{
    stop();
    m_mediaPlayer->setMedia(m_stations.value(m_station));
    m_mediaPlayer->play();
}

void WebRadioPlayer::stop()
{
    m_mediaPlayer->stop();
    m_restartTimer->stop();
}
