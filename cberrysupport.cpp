#include "cberrysupport.h"

#include <QCoreApplication>
#include <QTimerEvent>
#include <QWidget>
#include <QtDebug>

#ifdef HAVE_CBERRY
#include <bcm2835.h>
#include "cberry/tft.h"
#include "cberry/ST7789.h"
#endif

#include <exception>

CBerrySupport::CBerrySupport(QWidget *parent)
    : QObject(parent)
{
#ifdef HAVE_CBERRY
    if (!bcm2835_init())
        throw std::runtime_error("Failed to initialize the bcm2835 library");

    TFT_init_board();
    TFT_hard_reset();
    STcontroller_init();

    TFT_SetBacklightPWMValue( 16 );
#endif

//    installEventFilters(parent);
}

CBerrySupport::~CBerrySupport()
{
#ifdef HAVE_CBERRY
    bcm2835_close();
#endif
}

void CBerrySupport::renderToCBerry()
{
    qDebug() << "Rendering to image";
    QWidget *w = static_cast<QWidget*>(parent());
    QImage image(w->size(), QImage::Format_RGB16);
    w->render(&image);

    image.save("test.png");

#ifdef HAVE_CBERRY
    uint32_t len = image.size().width() * image.size().height();
    STcontroller_Write_Picture(reinterpret_cast<const uint16_t*>(image.constBits()), len);
#endif
}

void CBerrySupport::installEventFilters(QObject *obj)
{
    qDebug() << "Installing event filter on" << obj;
    obj->installEventFilter(this);
    for (QObject *child : obj->children()) {
        if (child == this || !child->isWidgetType())
            continue;

        installEventFilters(child);
    }
}

bool CBerrySupport::eventFilter(QObject *obj, QEvent *event)
{
    qDebug() << "filter:" << obj << event->type();

    if (event->type() == QEvent::UpdateRequest) {
        renderToCBerry();
        return true;
    }

    return QObject::eventFilter(obj, event);
}
