#ifndef WEBRADIOPLAYER_H
#define WEBRADIOPLAYER_H

#include <QObject>
#include <QMap>
#include <QMediaPlayer>
#include <QStringList>
#include <QUrl>

class QTimer;

class WebRadioPlayer : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QStringList stations READ stations)
    Q_PROPERTY(QString station READ station WRITE setStation NOTIFY stationChanged)
    Q_PROPERTY(QMediaPlayer::State state READ state NOTIFY stateChanged)
public:
    explicit WebRadioPlayer(QObject *parent = 0);

    QStringList stations() const;
    QString station() const;

    QMediaPlayer::State state() const;

signals:
    void stationChanged(const QString &station);
    void stateChanged(QMediaPlayer::State state);

public slots:
    void play();
    void stop();

    void setStation(const QString &station);

private slots:
    void handleError(QMediaPlayer::Error error);

private:
    QMap<QString, QUrl> m_stations;
    QString m_station;
    QMediaPlayer *m_mediaPlayer;
    QTimer *m_restartTimer;
};

#endif // WEBRADIOPLAYER_H
