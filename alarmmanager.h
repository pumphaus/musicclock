#ifndef ALARMMANAGER_H
#define ALARMMANAGER_H

#include <QDateTime>
#include <QMap>
#include <QObject>

class Alarm;

class QTimer;

class QStateMachine;

class AlarmManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QList<Alarm*> alarms READ alarms WRITE setAlarms NOTIFY alarmsChanged)
    Q_PROPERTY(QDateTime nextAlarm READ nextAlarm NOTIFY nextAlarmChanged)
    Q_PROPERTY(AlarmManager::State state READ state NOTIFY stateChanged)
public:
    enum State {
        Idle,
        Ring,
        Snooze
    };
    Q_ENUM(State)

    explicit AlarmManager(QObject *parent = 0);

    QList<Alarm*> alarms() const;

    void addAlarm(Alarm *alarm);
    void removeAlarm(Alarm *alarm);

    QDateTime nextAlarm() const;

    AlarmManager::State state() const;

public slots:
    void setAlarms(const QList<Alarm*> &alarms);

signals:
    void alarmsChanged(const QList<Alarm*> &alarms);
    void nextAlarmChanged(const QDateTime &nextAlarm);

    void stateChanged(AlarmManager::State state);

    void snooze();
    void idle();
    void ring();

private slots:
    void checkAlarmTimes();

private:
    QList<Alarm*> m_alarms;
    QMap<QDateTime, Alarm*> m_nextAlarmQueue;
    QTimer *m_alarmUpdateTimer, *m_snoozeTimer;

    QStateMachine *m_stateMachine;
    AlarmManager::State m_state;
};

#endif // ALARMMANAGER_H
