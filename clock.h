#ifndef CLOCK_H
#define CLOCK_H

#include <QImage>
#include <QTime>
#include <QWidget>

class QLabel;
class QSvgRenderer;

namespace Ui {
class Clock;
}

class Alarm;
class AlarmManager;
class PlayerBase;

class Clock : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(QTime time READ time WRITE setTime NOTIFY timeChanged)
public:
    explicit Clock(PlayerBase *player, AlarmManager *alarmManager, QWidget *parent = 0);
    ~Clock();

    QTime time() const;

public slots:
    void setTime(const QTime &time);
    void setNextAlarm(const QDateTime &nextAlarm);

signals:
    void timeChanged(const QTime &time);

protected:
    bool eventFilter(QObject *, QEvent *e);
    QGraphicsEffect *createDropShadowEffect(qreal offset = 2.0) const;
    void repositionAlarmIndicator();
    void repositionTitleLabel();

private:
    Ui::Clock *ui;
    QSvgRenderer *m_clockRenderer;
    QLabel *m_alarmIndicator, *m_titleLabel;
    QTime m_time;
    AlarmManager *m_alarmManager;
    PlayerBase *m_player;
};

#endif // CLOCK_H
