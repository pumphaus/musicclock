#include "alarm.h"
#include "alarmeditor.h"
#include "ui_alarmeditor.h"

#include <QProxyStyle>
#include <QKeyEvent>
#include <QLineEdit>
#include <QtDebug>

AlarmEditor::AlarmEditor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AlarmEditor),
    m_alarm(0)
{
    ui->setupUi(this);

    ui->dateTimeEdit->setReadOnly(true);
    ui->dateTimeEdit->installEventFilter(this);

    connect(ui->apply, &QAbstractButton::clicked, this, &AlarmEditor::apply);
    connect(ui->remove, &QAbstractButton::clicked, this, &AlarmEditor::removed);
}

AlarmEditor::~AlarmEditor()
{
    delete ui;
}

Alarm *AlarmEditor::alarm() const
{
    return m_alarm;
}

void AlarmEditor::setAlarm(Alarm *alarm)
{
    if (m_alarm == alarm)
        return;

    m_alarm = alarm;

    if (m_alarm != 0) {
        ui->dateTimeEdit->setTime(m_alarm->alarmTime());
        ui->chkActive->setChecked(m_alarm->isActive());
        ui->chkMonday->setChecked(m_alarm->weekdays() & Alarm::Monday);
        ui->chkTuesday->setChecked(m_alarm->weekdays() & Alarm::Tuesday);
        ui->chkWednesday->setChecked(m_alarm->weekdays() & Alarm::Wednesday);
        ui->chkThursday->setChecked(m_alarm->weekdays() & Alarm::Thursday);
        ui->chkFriday->setChecked(m_alarm->weekdays() & Alarm::Friday);
        ui->chkSaturday->setChecked(m_alarm->weekdays() & Alarm::Saturday);
        ui->chkSunday->setChecked(m_alarm->weekdays() & Alarm::Sunday);
    }

    emit alarmChanged(alarm);
}

void AlarmEditor::apply()
{
    if (m_alarm) {
        m_alarm->setAlarmTime(ui->dateTimeEdit->time());
        m_alarm->setActive(ui->chkActive->checkState() == Qt::Checked);

        Alarm::Weekdays weekdays = Alarm::None;
        if (ui->chkMonday->checkState() == Qt::Checked) {
            weekdays |= Alarm::Monday;
        }
        if (ui->chkTuesday->checkState() == Qt::Checked) {
            weekdays |= Alarm::Tuesday;
        }
        if (ui->chkWednesday->checkState() == Qt::Checked) {
            weekdays |= Alarm::Wednesday;
        }
        if (ui->chkThursday->checkState() == Qt::Checked) {
            weekdays |= Alarm::Thursday;
        }
        if (ui->chkFriday->checkState() == Qt::Checked) {
            weekdays |= Alarm::Friday;
        }
        if (ui->chkSaturday->checkState() == Qt::Checked) {
            weekdays |= Alarm::Saturday;
        }
        if (ui->chkSunday->checkState() == Qt::Checked) {
            weekdays |= Alarm::Sunday;
        }

        m_alarm->setWeekdays(weekdays);
    }

    emit applied();
}

void AlarmEditor::hideEvent(QHideEvent *)
{
    emit hidden();
}

void AlarmEditor::focusInEvent(QFocusEvent *)
{
    ui->dateTimeEdit->setFocus();
}

bool AlarmEditor::eventFilter(QObject *obj, QEvent *e)
{
    QDateTimeEdit *dtEdit = qobject_cast<QDateTimeEdit*>(obj);
    if (!dtEdit) {
        return false;
    }

    if (e->type() == QEvent::KeyPress) {
        QKeyEvent *ke = static_cast<QKeyEvent*>(e);
        if (ke->key() == Qt::Key_Left) {
            qApp->postEvent(dtEdit, new QKeyEvent(QEvent::KeyPress, Qt::Key_Tab, Qt::ShiftModifier));
            return true;
        } else if (ke->key() == Qt::Key_Right) {
            qApp->postEvent(dtEdit, new QKeyEvent(QEvent::KeyPress, Qt::Key_Tab, Qt::NoModifier));
            return true;
        } else if (ke->key() == Qt::Key_Up) {
            dtEdit->stepUp();
            return true;
        } else if (ke->key() == Qt::Key_Down) {
            dtEdit->stepDown();
            return true;
        }
    }

    return false;
}
