#include "failsafeplayer.h"

#include "alarmmanager.h"

#include <QFileInfo>
#include <QMediaPlaylist>
#include <QTimer>
#include <QUrl>

FailsafePlayer::FailsafePlayer(AlarmManager *manager, PlayerBase *player, QObject *parent)
    : PlayerBase(parent), m_alarmManager(manager), m_player(player),
      m_fallbackPlayer(new QMediaPlayer(this))
{
    connect(m_player, &PlayerBase::titleChanged, this, &FailsafePlayer::titleChanged);
    connect(m_player, &PlayerBase::stateChanged, this, &FailsafePlayer::stateChanged);
    connect(m_player, &PlayerBase::stateChanged, this, &FailsafePlayer::checkFallbackAlarm);

    QMediaPlaylist *playlist = new QMediaPlaylist(m_fallbackPlayer);
    playlist->addMedia(QUrl::fromLocalFile(QFileInfo("alarm.mp3").absoluteFilePath()));
    playlist->setPlaybackMode(QMediaPlaylist::Loop);
    m_fallbackPlayer->setPlaylist(playlist);
}

PlayerBase::State FailsafePlayer::state() const
{
    return m_player->state();
}

QString FailsafePlayer::title() const
{
    if (m_player->state() == Playing)
        return m_player->title();
    else
        return QString();
}

void FailsafePlayer::play()
{
    qDebug() << "Failsafe play...";
    m_player->play();
    QTimer::singleShot(3000, this, &FailsafePlayer::checkFallbackAlarm);
}

void FailsafePlayer::pause()
{
    m_player->pause();
}

void FailsafePlayer::stop()
{
    m_player->stop();
    m_fallbackPlayer->stop();
}

void FailsafePlayer::checkFallbackAlarm()
{
    if (m_player->state() != Playing && m_alarmManager->state() == AlarmManager::Ring) {
        m_player->stop();
        m_fallbackPlayer->play();
    }
}
