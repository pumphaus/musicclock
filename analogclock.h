#ifndef ANALOGCLOCK_H
#define ANALOGCLOCK_H

#include <QTime>
#include <QWidget>

class QSvgRenderer;

class AnalogClock : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(QSvgRenderer* renderer READ renderer WRITE setRenderer NOTIFY rendererChanged)
    Q_PROPERTY(QTime time READ time WRITE setTime NOTIFY timeChanged)
    Q_PROPERTY(bool secondHandVisible READ secondHandVisible WRITE setSecondHandVisible NOTIFY secondHandVisibleChanged)
public:
    explicit AnalogClock(QWidget *parent = 0);
    explicit AnalogClock(QSvgRenderer *renderer, QWidget *parent = 0);

    QSvgRenderer *renderer() const;

    QSize sizeHint() const;
    QTime time() const;
    bool secondHandVisible() const;

signals:
    void rendererChanged(QSvgRenderer *renderer);
    void timeChanged(const QTime &time);
    void secondHandVisibleChanged(bool secondHandVisible);

public slots:
    void setRenderer(QSvgRenderer *renderer);
    void setTime(const QTime &time);
    void setSecondHandVisible(bool secondHandVisible);

protected:
    void paintEvent(QPaintEvent *);
    void drawHand(QPainter &p, const QString& id, qreal rot = 0, qreal topMargin = 0);

private:
    QSvgRenderer *m_renderer;
    QTime m_time;
    bool m_secondHandVisible;
};

#endif // ANALOGCLOCK_H
