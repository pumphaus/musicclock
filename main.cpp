#include "clock.h"
#include "musicclockappwidget.h"

#include <QApplication>
#include <QtDebug>

#include "alarmmanager.h"
#include "alarm.h"
#include "failsafeplayer.h"
#include "mpdclient.h"

#include <QTimer>

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("Arno Rehn");
    QCoreApplication::setOrganizationDomain("arnorehn.de");
    QCoreApplication::setApplicationName("MusicClock");

    QApplication a(argc, argv);
    a.setFont(QFont("Noto Sans"));

    AlarmManager manager;

    MPDClient client;
    FailsafePlayer failsafePlayer(&manager, &client);

    QObject::connect(&manager, &AlarmManager::stateChanged,
                     &failsafePlayer, [&failsafePlayer](AlarmManager::State state)
    {
        if (state == AlarmManager::Ring) {
            failsafePlayer.play();
        } else {
            failsafePlayer.stop();
        }
    });

    MusicClockAppWidget musicClock(&client, &manager);
    musicClock.show();

    Alarm *alarm = new Alarm(&manager);
    alarm->setActive(true);
    alarm->setWeekdays(Alarm::WorkingDays);
    manager.addAlarm(alarm);

    return a.exec();
}
