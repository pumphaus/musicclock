#include "clockmenu.h"

#include <QAction>
#include <QCoreApplication>
#include <QEvent>
#include <QKeyEvent>
#include <QGraphicsDropShadowEffect>
#include <QPushButton>
#include <QToolButton>
#include <QVBoxLayout>

#include <QtDebug>

ClockMenu::ClockMenu(QWidget *parent) :
    QWidget(parent), m_layout(new QVBoxLayout(this))
{
    setAutoFillBackground(false);
    repopulateButtons();
}

ClockMenu::~ClockMenu()
{
}

void ClockMenu::repopulateButtons()
{
    while (m_layout->count() > 0) {
        QLayoutItem *item = m_layout->itemAt(0);
        m_layout->removeItem(item);
        if (item->widget()) {
            delete item->widget();
        }
        delete item;
    }

    for (QAction *action : actions()) {
        QToolButton *button = new QToolButton(this);
        button->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        button->setAutoRaise(true);
        button->setDefaultAction(action);

        QGraphicsDropShadowEffect *dropShadow = new QGraphicsDropShadowEffect(button);
        dropShadow->setBlurRadius(3);
        dropShadow->setOffset(1.4);
        button->setGraphicsEffect(dropShadow);
        m_layout->addWidget(button);
    }

    m_layout->addStretch();
}

void ClockMenu::focusInEvent(QFocusEvent *)
{
    m_layout->itemAt(0)->widget()->setFocus();
    qApp->sendEvent(m_layout->itemAt(0)->widget(), new QKeyEvent(QEvent::KeyPress, Qt::Key_Down, Qt::NoModifier));
    qApp->sendEvent(m_layout->itemAt(0)->widget(), new QKeyEvent(QEvent::KeyPress, Qt::Key_Up, Qt::NoModifier));
}
