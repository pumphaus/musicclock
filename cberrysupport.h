#ifndef CBERRYSUPPORT_H
#define CBERRYSUPPORT_H

#include <QObject>

class CBerrySupport : public QObject
{
    Q_OBJECT
public:
    explicit CBerrySupport(QWidget *parent = 0);
    ~CBerrySupport();

protected:
    void renderToCBerry();

    void installEventFilters(QObject *obj);
    bool eventFilter(QObject *obj, QEvent *event);
};

#endif // CBERRYSUPPORT_H
